/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

import java.util.ArrayList;

/**
 * Pakettivarasto. Kaikki valmiit käsittelyn läpi päässeet paketit tulevat tänne.
 * ArrayListissä taas vaihteeksi laskurin kera. Samaa kaavaa kierrätän pitkälti.
 * @author Petri
 */
public class Storage {
    
    private ArrayList<Pack> storageArea = new ArrayList<>();
    static private int storageSpace;
    
    static private Storage store = new Storage();
    
    private Storage() {
    }
    
    static public Storage getInstance() {
        return store;
    }
    public void addToStorage(Pack a) {
        storageArea.add(a);
        storageSpace++;
    }
    
    public Pack getPackage(String demand) {
        int z = 0;
        int n;
        String name;
        for (n = 0; n < storageSpace; n++) {
            Pack item = storageArea.get(n);
            name = item.toString();
            if (name.equals(demand)) {
                return item;
            }
            
        }
        Pack item = null;
        return item;
    }
    
    public ArrayList getPackageList() {
        return storageArea;
    }
    
    public int getPackageCount() {
        return storageSpace;
    }
    
}
