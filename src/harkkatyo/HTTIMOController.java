/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 * Hallitsee TIMOn toiminnallisuutta. 
 * @author Petri
 */
public class HTTIMOController implements Initializable {
    @FXML
    private WebView webMap;
    @FXML
    private Button addSP;
    @FXML
    private Button createPack;
    @FXML
    private Button updatePack;
    @FXML
    private Button sendPack;
    @FXML
    private Button emptyPath;
    @FXML
    private ComboBox<String> pickPack;
    @FXML
    private ComboBox<String> spChoose;
    

   
    // Alustaa kartan ja paikkaluettelon
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        
        webMap.getEngine().load(getClass().getResource("index.html").toExternalForm());
        
        
        try {
            URL autoUrl = new URL("http://smartpost.ee/fi_apt.xml");
            
            BufferedReader br  = new BufferedReader(new InputStreamReader(autoUrl.openStream()));
            
            String content = "";
            String line;
            
            while((line = br.readLine()) != null) {
                content += line + "\n";
            }
            
            Postal p = new Postal(content);
            
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(HTTIMOController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HTTIMOController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        PlacesDB places = PlacesDB.getInstance();
        ArrayList <SmartPost> placeList = places.getPlaceList();
        int placeCount = places.getPlaceCount();
        String before = "temp";
        String city;
        
        for (int i = 0; i < placeCount; i++) {
            city = placeList.get(i).getCity();
            if (before.equals(city))
                continue;
            spChoose.getItems().add(city);
            before = city;
        }
        
        
    }    

    // Lisää valitun paikan kartalle, poistaa paikkalistasta, lisää 
    // pakettien lähetystieto listaan
    @FXML
    private void addSP(ActionEvent event) {
        String scriptBeg = "document.goToLocation('";
        String city = (String)spChoose.getSelectionModel().getSelectedItem();
        
        PlacesDB places = PlacesDB.getInstance();
        int placeCount = places.getPlaceCount();
        ArrayList<SmartPost> placeList = places.getPlaceList();
        ArrayList<SmartPost> mapList = places.getMapList();
        
        for(int n = 0; n < placeCount; n++) {
            if (placeList.get(n).getCity().equals(city)) {
                places.addToMaps(placeList.get(n));
                places.removePlace(n);
                placeCount--;
                n--;
            }
        }
        
        int mapCount = places.getMapCount();
        
        for (int i = 0; i < mapCount; i++) {
            if(city.equals(mapList.get(i).getCity())) {
                String location = mapList.get(i).getLoca();
                String code = mapList.get(i).getCode();
                String post = mapList.get(i).getPost();
                String availability = mapList.get(i).getAvailability();
                String script = scriptBeg+location+", "+code+", "+city+"','"+post+" avoinna "+availability+"','red')";
                
                webMap.getEngine().executeScript(script);
            }
        }
        
        spChoose.valueProperty().set(null);
        spChoose.getItems().clear();
        String before = "temp";
        for (int i = 0; i < placeCount; i++) {
            city = placeList.get(i).getCity();
            if (before.equals(city))
                continue;
            spChoose.getItems().add(city);
            before = city;
        }
        
    }

    // Luo paketin
    @FXML
    private void createPack(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        Scene scene = new Scene(root);
        stage.setResizable(true);
        
        stage.setScene(scene);
        stage.show();
        
        
    }

    // Itse asiassa ei mitään hajua onko tämä edes tarpeellinen nappula
    // Tällainen oli siinä esimerkissä joten lykkäsin sille tällaisen toiminnallisuuden
    // tldr: voi päivittää pakettilistan uuden paketin luonnin jälkeen
    @FXML
    private void updatePack(ActionEvent event) {
        Storage store = Storage.getInstance();
        ArrayList<Pack> packageList = store.getPackageList();
        int packCount = store.getPackageCount();
        
        pickPack.valueProperty().set(null);
        pickPack.getItems().clear();
        for (int n = 0; n < packCount; n++) {
            Pack item = packageList.get(n);
            pickPack.getItems().add(item.toString());
        }
    }

    // Piirtää reitin
    @FXML
    private void sendPack(ActionEvent event) {
        Storage store = Storage.getInstance();
        String name = (String)pickPack.getSelectionModel().getSelectedItem();
        if (name == null) {
            return;
        }
        Pack beingSent = store.getPackage(name);
        ArrayList<Float> route = beingSent.getRoute();
        int packClass = beingSent.getPackClass();
        double distanceTravelled = (double)webMap.getEngine().executeScript("document.createPath("+ route +", 'red', "+String.valueOf(packClass)+")");
    }

    // Tyhjentää reitin
    @FXML
    private void emptyPath(ActionEvent event) {
        webMap.getEngine().executeScript("document.deletePaths()");
    }


}
