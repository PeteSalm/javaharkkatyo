/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * Tämä sisältää paketinluonti-ikkunan toiminnallisuuden.
 * @author Petri
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private Label errorText;
    @FXML
    private ComboBox<Item> itemBox;
    
    
    @FXML
    private TextField itemName;
    @FXML
    private TextField itemSize;
    @FXML
    private TextField itemMass;
    @FXML
    private CheckBox isFragile;
   
    
    @FXML
    private RadioButton firstClass;
    @FXML
    private RadioButton secondClass;
    @FXML
    private RadioButton thirdClass;
    
    @FXML
    private Button infoButton;
    
    
    @FXML
    private ComboBox<String> startCity;
    @FXML
    private ComboBox<String> startAuto;
    @FXML
    private ComboBox<String> endCity;
    @FXML
    private ComboBox<String> endAuto;
    
    
    @FXML
    private Button cancel;
    @FXML
    private Button createPackage;
    @FXML
    private WebView webMap;

    // Valmistelee preset-esineet ja paikkakuntavalikot. tuo webMap liittyy matkan mittaukseen
    // ja on erittäin kyseenalainen, mutta näillä näkymin on toiminut. Lisähuomautuksena tuo WebView
    // on kutistettu näkymättömiin, minkä takia sitä ei näy.
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        webMap.getEngine().load(getClass().getResource("index.html").toExternalForm());
        
        Item Kirja = new Item("Kirja", "30 30 30", "1", 0);
        Item Peli = new Item("Peli", "10 10 10", "0.3", 1);
        Item Vaasi = new Item("Vaasi", "50 10 30", "3", 1);
        Item Tuoli = new Item("Tuoli", "70 30 40", "8", 0);
        Item Oma = new Item("Luo oma", "0 0 0", "0", 0);
        itemBox.getItems().add(Kirja);
        itemBox.getItems().add(Peli);
        itemBox.getItems().add(Vaasi);
        itemBox.getItems().add(Tuoli);
        itemBox.getItems().add(Oma);
        
        
        PlacesDB maps = PlacesDB.getInstance();
        ArrayList<SmartPost> mapList = maps.getMapList();
        int mapCount = maps.getMapCount();
        
        String before = "temp";
        
        for (int n = 0; n < mapCount; n++) {
            String city = mapList.get(n).getCity();
            if (before.equals(city))
                continue;
            startCity.getItems().add(city);
            endCity.getItems().add(city);
            before = city;
        }
    }    

    //Luokkanappuloiden valinta
    @FXML
    private void isFirst(ActionEvent event) {
        secondClass.setSelected(false);
        thirdClass.setSelected(false);
    }

    @FXML
    private void isSecond(ActionEvent event) {
        firstClass.setSelected(false);
        thirdClass.setSelected(false);
    }

    @FXML
    private void isThird(ActionEvent event) {
        secondClass.setSelected(false);
        firstClass.setSelected(false);
    }

    //Info luokista
    @FXML
    private void showInfo(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("HTinfo.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }

    // Vaihtaa automaatteja kaupungin valinnan mukaan
    @FXML
    private void pickStartPoint(ActionEvent event) {
        
        startAuto.valueProperty().set(null);
        startAuto.getItems().clear();
        String city = (String)startCity.getSelectionModel().getSelectedItem();
        
        PlacesDB places = PlacesDB.getInstance();
        ArrayList <SmartPost> mapList = places.getMapList();
        int mapCount = places.getMapCount();
        
        String currCity;
        for (int i = 0; i < mapCount; i++) {
            currCity = mapList.get(i).getCity();
            if (city.equals(currCity)) {
                startAuto.getItems().add(mapList.get(i).getPost());
            }
        }
        
    }

    // Sama kuin ylempi
    @FXML
    private void pickEndCity(ActionEvent event) {
        
        endAuto.valueProperty().set(null);
        endAuto.getItems().clear();
        String city = (String)endCity.getSelectionModel().getSelectedItem();
        
        PlacesDB places = PlacesDB.getInstance();
        ArrayList <SmartPost> mapList = places.getMapList();
        int mapCount = places.getMapCount();
        
        String currCity;
        for (int i = 0; i < mapCount; i++) {
            currCity = mapList.get(i).getCity();
            if (city.equals(currCity)) {
                endAuto.getItems().add(mapList.get(i).getPost());
            }
        } 
    }

    // Sulkee ikkunan luomatta pakettia
    @FXML
    private void cancelCreate(ActionEvent event) {
        ((Node)(event.getSource())).getScene().getWindow().hide();
    }

    //Luo paketin annetuista tiedoista. Sisältää virheentarkastukset if-lauseilla.
    //Tulostaa virheen nappuloiden alle, jotta käyttäjä tietää mitä korjata.
    @FXML
    private void createPackage(ActionEvent event) {
        
        String route;
        
        
        Pack newPackage = null;
        String sC = (String)startCity.getSelectionModel().getSelectedItem();
        String eC = (String)endCity.getSelectionModel().getSelectedItem();
        String sA = (String)startAuto.getSelectionModel().getSelectedItem();
        String eA = (String)endAuto.getSelectionModel().getSelectedItem();
        
        if (sC.equals(eC) && sA.equals(eA)) {
            errorText.setText(null);
            errorText.setText("Lähtöpaikka ja määränpää samat!");
            return;
        }
        
        if (itemBox.valueProperty().get() == null) {
            errorText.setText(null);
            errorText.setText("Valitse jokin esine!");
            return;
        }
        
        if((itemBox.valueProperty().get().getName().equals("Luo oma"))) {
            if((itemName.getText().equals("")) || (itemSize.getText().equals("")) || (itemMass.getText().equals(""))) {
                errorText.setText(null);
                errorText.setText("Valitse jokin esine!");
                return;
            }
        }
        
        if ((firstClass.isSelected() == false) && (secondClass.isSelected() == false) && (thirdClass.isSelected() == false)) {
            errorText.setText(null);
            errorText.setText("valitse jokin luokka!");
            return;
        }
        
        if ((sC == null) || (eC == null) || (sA == null) || (eA == null)) {
            errorText.setText(null);
            errorText.setText("Täytä lähetystiedot!");
            return;
        }
        
        String sLat = "";
        String sLon = "";
        String eLat = "";
        String eLon = "";
        
        
        String currCity;
        String currAuto;
        
        PlacesDB places = PlacesDB.getInstance();
        ArrayList <SmartPost> mapList = places.getMapList();
        int mapCount = places.getMapCount();
        
        for (int i = 0; i < mapCount; i++) {
            currCity = mapList.get(i).getCity();
            currAuto = mapList.get(i).getPost();
            if (sC.equals(currCity) && sA.equals(currAuto)) {
                sLat = mapList.get(i).getLat();
                sLon = mapList.get(i).getLon();
            }
            if (eC.equals(currCity) && eA.equals(currAuto)) {
                eLat = mapList.get(i).getLat();
                eLon = mapList.get(i).getLon();
            }
        }
            
            
        ArrayList <Float> al = new ArrayList <> ();
        al.add(Float.valueOf(sLat));
        al.add(Float.valueOf(sLon));
        al.add(Float.valueOf(eLat));
        al.add(Float.valueOf(eLon));
        
        if (itemBox.valueProperty().get().getName().equals("Luo oma")) {
            int f;
            if (isFragile.isSelected()) {
                f = 1;
            } else {
                f = 0;
            }       
            Item newItem = new Item(itemName.getText(), itemSize.getText(), itemMass.getText(), f);
            
            int packClass = 0;
            if (firstClass.isSelected()) {
                packClass = 1;
            }
            if (secondClass.isSelected()) {
                packClass = 2;
            }
            if (thirdClass.isSelected()) {
                packClass = 3;
            }
            
            String packName = "Paketti";
            
            newPackage = new Pack(packClass, packName, newItem, al);
            
        }
        else {
            int packClass = 0;
            if (firstClass.isSelected()) {
                packClass = 1;
            }
            if (secondClass.isSelected()) {
                packClass = 2;
            }
            if (thirdClass.isSelected()) {
                packClass = 3;
            }
            
            String packName = "Paketti";
            
            newPackage = new Pack(packClass, packName, itemBox.valueProperty().get(), al);   
        }
        
        int packClass = newPackage.getPackClass();
        
        String size = newPackage.getPackItem().getSize();
        Scanner scanner = new Scanner(size);
        int volume = 1;
        int i;
        for (i = 0; scanner.hasNext(); i++) {
            int a = Integer.valueOf(scanner.next());
            volume = volume*a;
        }
        scanner.close();
        
        
        if ((i != 3) || (volume <= 0)) {
            errorText.setText(null);
            errorText.setText("Virheellinen koko!");
            newPackage.decreaseCount();
            return;
        }
        
        float mass = Float.valueOf(newPackage.getPackItem().getMass());
        mass = mass*100;
        if (mass <= 1) {
            errorText.setText(null);
            errorText.setText("Liian kevyt!");
            newPackage.decreaseCount();
            return;
        }
        
        int frag = newPackage.getPackItem().getFrag();
        ArrayList<Float> road = newPackage.getRoute();
        
        
        if (packClass == 1) {
            if (frag == 1) {
                errorText.setText(null);
                errorText.setText("Ei särkyviä tässä luokassa!");
                newPackage.decreaseCount();
                return;
            }
            double distanceTravelled = (double)webMap.getEngine().executeScript("document.createPath("+ road +", 'red', "+String.valueOf(packClass)+")");
            if (distanceTravelled > 150) {
                errorText.setText(null);
                errorText.setText("Matka yli 150km!");
                newPackage.decreaseCount();
                return;
            }
        }
        
        if (packClass == 2) {
            if (volume > 125000) {
                errorText.setText(null);
                errorText.setText("Liian iso tähän luokkaan!");
                newPackage.decreaseCount();
                return;
            }
            if (mass > 1000) {
                errorText.setText(null);
                errorText.setText("Liian painava tähän luokkaan!");
                newPackage.decreaseCount();
                return;
            }
        }
        
        if (packClass == 3) {
            if (frag == 1) {
                errorText.setText(null);
                errorText.setText("Ei särkyviä tässä luokassa!");
                newPackage.decreaseCount();
                return;
            }
            if (volume < 125000) {
                errorText.setText(null);
                errorText.setText("Liian pieni tähän luokkaan!");
                newPackage.decreaseCount();
                return;
            }
            if (mass < 1000) {
                errorText.setText(null);
                errorText.setText("Liian kevyt tähän luokkaan!");
                newPackage.decreaseCount();
                return;
            }
        }
        
        
        Storage store = Storage.getInstance();
        store.addToStorage(newPackage);
        
        ((Node)(event.getSource())).getScene().getWindow().hide();
    }

    
}
