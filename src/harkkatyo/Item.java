/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

/**
 * Esine-luokka
 * Sisältää tiedot esineista ja välineet tietojen noutamiseen.
 * @author Petri
 */
public class Item {
    
    private String name;
    private String size;
    private String mass;
    private int fragile;
    
    public Item(String a, String b, String c, int d) {
        name = a;
        size = b;
        mass = c;
        fragile = d;
    }
    
    public String getName() {
        return name;
    }
    
    public String getSize() {
        return size;
    }
    
    public String getMass() {
        return mass;
    }
    
    public int getFrag() {
        return fragile;
    }
    
    @Override
    public String toString() {
        return name;
    }
}
