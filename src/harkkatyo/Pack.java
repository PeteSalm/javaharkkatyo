/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

import java.util.ArrayList;

/**
 * Paketti luokka, paketit sisältävät esineen, luokan sekä reittitiedon (+nimen)
 * Sisältää tarvittavat noutotoiminnot
 * @author Petri
 */
public class Pack {
    
    private int packClass;
    private String name;
    private Item item;
    private ArrayList<Float> route;
    private static int packId;
    
    
    public Pack(int a, String b, Item c, ArrayList d) {
            packClass = a;
            name = b+" "+String.valueOf(packId+1);
            item = c;
            route = d;
            packId++;
    }
    
    public ArrayList getRoute() {
        return route;
    }
    
    public int getPackClass() {
        return packClass;
    }
    
    public Item getPackItem() {
        return item;
    }
    
    public void decreaseCount() {
        packId--;
    }
    
    @Override
    public String toString() {
        return name;
    }
}
