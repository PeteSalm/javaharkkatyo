/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

/**
 * SmartPost-luokka SmartPost-olioiden tekoon. 
 * Oliot sisältävät kaikki tarvittavat tiedot.
 * Sisältää myös getterit kaikelle.
 * @author Petri
 */
public class SmartPost {
    
    private String city;
    private String loca;
    private String code;
    private String post;
    private String availability;
    private String lat;
    private String lon;
    
    public SmartPost(String a, String b, String c, String d, String e, String f, String g) {
        city = a;
        loca = b;
        code = c;
        post = d;
        availability = e;
        lat = f;
        lon = g;
    }
    
    public String getCity() {
        return city;
    }
    
    public String getLoca() {
        return loca;
    }
    
    public String getCode() {
        return code;
    }
    
    public String getPost() {
        return post;
    }
    
    public String getAvailability() {
        return availability;
    }
    
    public String getLat() {
        return lat;
    }
    
    public String getLon() {
        return lon;
    }
    
}
