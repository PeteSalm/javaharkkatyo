/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

import java.util.ArrayList;

/**
 * Paikka database. Sisältää luettelon paikoista sekä luettelon kartalle piirretyistä paikoista.
 * Eli siis kaksi arraylistaa (SmartPosteista), josta paikkoja piirrettäessä liikkuu alkiot 
 * placeLististä mapListiin. Sisältää laskurit ja tarvittavat metodit.
 * @author Petri
 */
public class PlacesDB {
    
    private ArrayList<SmartPost> placeList = new ArrayList<>();
    static private int placeCount;
    private ArrayList<SmartPost> mapList = new ArrayList<>();
    static private int mapCount;
    
    static private PlacesDB places = new PlacesDB();
    
    private PlacesDB() {
    }
    
    static public PlacesDB getInstance() {
        return places;
    }
    
    public void addToPlaces(SmartPost sp) {
        placeList.add(sp);
        placeCount++;
    }
    
    public ArrayList getPlaceList() {
        return placeList;
    }
    
    public int getPlaceCount() {
        return placeCount;
    }
    
    public void removePlace(int a) {
        placeList.remove(a);
        placeCount = placeCount-1;
    }
    
    public void addToMaps(SmartPost sp) {
        mapList.add(sp);
        mapCount++;
    }
    
    public ArrayList getMapList() {
        return mapList;
    }
    
    public int getMapCount() {
        return mapCount;
    }
    
    
}
