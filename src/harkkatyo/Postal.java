/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

import java.io.IOException;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Lukee XML-tiedostoa ja kasaa tiedoista SmartPost-olioita.
 * Tehty pitkälti luentoesimerkkien pohjalta.
 * @author Petri
 */
public class Postal {
    
    private Document doc;

    public Postal(String content) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
            
            parseCurrentData();
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(Postal.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private void parseCurrentData() {
        NodeList nodes = doc.getElementsByTagName("place");
        PlacesDB places = PlacesDB.getInstance();
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            
            SmartPost sp = new SmartPost(getValue("city", e), getValue("address", e), getValue("code", e), getValue("postoffice", e), getValue("availability", e), getValue("lat", e), getValue("lng", e));
            places.addToPlaces(sp);
            
        }
    }
    
    private String getValue(String tag, Element e) {
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
    }
}
